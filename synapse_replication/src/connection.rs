use std::collections::HashMap;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::task::Poll;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use tokio::net::TcpStream;
use tokio::sync::Mutex;
use tokio::time::{timeout, Delay};

use tokio_util::codec::{Framed, LinesCodec, LinesCodecError};

use futures::sink::SinkExt;
use futures::stream::{SplitSink, SplitStream, Stream, StreamExt};

use failure::{Error, Fail};

use crate::commands::{Command, Commands, NameCommand, PingCommand, PositionCommand, RDataCommand, ReplicateCommand, RemoteServerUpCommand};
use crate::streams::{EventStreamRow, FederationStreamRow, StreamRows};

type ReplicationStreamReader = SplitStream<Framed<TcpStream, LinesCodec>>;
type ReplicationStreamWriter = SplitSink<Framed<TcpStream, LinesCodec>, String>;

#[derive(Debug, Fail, PartialEq)]
pub enum ConnectionError {
    #[fail(display = "replication tcp timed out")]
    TcpTimeout,
    #[fail(display = "replication tcp disconnected")]
    TcpDisconnected,
    #[fail(display = "parse command line error")]
    ParseError,
}

#[derive(Debug)]
pub enum RowsData {
    Event(Vec<EventStreamRow>),
    Federation(Vec<FederationStreamRow>),
    Unknown(Vec<StreamRows>),
}

impl RowsData {
    fn from_stream_rows_vec(stream_rows_vec: Vec<StreamRows>) -> Self {
        if stream_rows_vec.is_empty() {
            return Self::Unknown(Vec::new());
        }

        if let StreamRows::Event(event) = stream_rows_vec.get(0).unwrap() {
            let mut rows: Vec<EventStreamRow> = Vec::new();

            for row in stream_rows_vec {
                if let StreamRows::Event(event) = row {
                    rows.push(event);
                }
            }

            return Self::Event(rows);
        }

        if let StreamRows::Federation(event) = stream_rows_vec.get(0).unwrap() {
            let mut rows: Vec<FederationStreamRow> = Vec::new();

            for row in stream_rows_vec {
                if let StreamRows::Federation(federation) = row {
                    rows.push(federation);
                }
            }

            return Self::Federation(rows);
        }

        Self::Unknown(stream_rows_vec)
    }
}

#[derive(Debug)]
pub struct ReplicationData {
    pub stream: String,
    pub token: u64,
    pub rows_data: RowsData,
}

#[derive(Debug)]
pub enum ReplicationLine {
    RData(ReplicationData),
    Position(PositionCommand),
    RemoteUp(RemoteServerUpCommand),
    Other(Commands),
}

pub struct Connection {
    name: String,
    server_name: String,

    host: String,
    port: u16,

    replication_reader: ReplicationStreamReader,
    replication_writer: Arc<Mutex<ReplicationStreamWriter>>,

    ping_status: Arc<AtomicBool>,

    pending_batches: HashMap<String, Vec<StreamRows>>,
}

impl Connection {
    pub async fn new(
        name: String,
        server_name: String,
        host: String,
        port: u16,
    ) -> Result<(Self, impl std::future::Future<Output = ()>), ConnectionError> {
        let replication_addr = format!("{}:{}", host, port);

        let mut stream: TcpStream =
            timeout(Duration::from_secs(5), TcpStream::connect(replication_addr))
                .await
                .map_err(|e| ConnectionError::TcpTimeout)?
                .map_err(|e| ConnectionError::TcpDisconnected)?;
        let mut line_stream = Framed::new(stream, LinesCodec::new());
        let (write_line_stream, read_line_stream) = line_stream.split();
        let write_line_stream_arc = Arc::new(Mutex::new(write_line_stream));
        let ping_status = Arc::new(AtomicBool::new(true));

        let background = Self::ping(write_line_stream_arc.clone(), ping_status.clone());

        let c = Connection {
            name,
            server_name,
            host,
            port,

            replication_reader: read_line_stream,
            replication_writer: write_line_stream_arc,

            ping_status,

            pending_batches: HashMap::new(),
        };

        Ok((c, background))
    }

    pub async fn subscribe(&mut self, stream_name: String, token: u64) {
        let sub_command = ReplicateCommand {
            stream: stream_name,
            token,
        };

        self.replication_writer
            .lock()
            .await
            .send(sub_command.to_string())
            .await
            .unwrap();
    }

    pub async fn send_command(&mut self, command: impl Command) {
        self.replication_writer
            .lock()
            .await
            .send(command.to_string())
            .await
            .unwrap();
    }

    async fn ping(writer: Arc<Mutex<ReplicationStreamWriter>>, status: Arc<AtomicBool>) {
        while status.load(Ordering::SeqCst) {
            tokio::time::delay_for(Duration::from_secs(5)).await;

            let time_since_epoch = SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards");

            let ping_command = PingCommand {
                millis_since_epoch: time_since_epoch.as_millis() as u64,
            };

            writer
                .lock()
                .await
                .send(ping_command.to_string())
                .await
                .unwrap();
        }
    }
}

impl Stream for Connection {
    type Item = Result<ReplicationLine, ConnectionError>;

    fn poll_next(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        let result: Option<_> = futures::ready!(self.replication_reader.poll_next_unpin(cx));

        match result {
            Some(Ok(line)) => {
                let command = Commands::from_line(line);

                let replication_line = match command {
                    Commands::RData(rdata) => {
                        if rdata.is_batched() {
                            if !self.pending_batches.contains_key(&rdata.stream) {
                                self.pending_batches
                                    .insert(rdata.stream.clone(), Vec::new());
                            }
                            self.pending_batches
                                .get_mut(&rdata.stream)
                                .unwrap()
                                .push(rdata.row_data.clone());

                            return self.poll_next(cx);
                        }

                        let mut rows: Vec<StreamRows> = Vec::new();

                        // Append pending rdata with current rdata.
                        if self.pending_batches.contains_key(&rdata.stream) {
                            let mut pending = self.pending_batches.remove(&rdata.stream).unwrap();
                            rows.append(&mut pending);
                        }

                        rows.push(rdata.row_data.clone());

                        let replication_data = ReplicationData {
                            stream: rdata.stream,
                            token: rdata.token.unwrap(),
                            rows_data: RowsData::from_stream_rows_vec(rows),
                        };

                        return Poll::Ready(Some(Ok(ReplicationLine::RData(replication_data))));
                    }
                    Commands::Position(pos) => ReplicationLine::Position(pos),
                    Commands::RemoteServerUp(rsu) => ReplicationLine::RemoteUp(rsu),
                    _ => ReplicationLine::Other(command),
                };

                Poll::Ready(Some(Ok(replication_line)))
            }

            Some(Err(e)) => Poll::Ready(Some(Err(ConnectionError::ParseError))),

            None => Poll::Ready(None),
        }
    }
}

impl Drop for Connection {
    fn drop(&mut self) {
        self.ping_status.store(false, Ordering::SeqCst);
    }
}
