use synapse_replication_derive::CommandDisplay;

use crate::streams::{EventStreamRow, FederationStreamRow, StreamRow, StreamRows};

#[derive(Debug, Clone)]
pub enum Commands {
    Server(ServerCommand),
    RData(RDataCommand),
    Position(PositionCommand),
    Error(ErrorCommand),
    Ping(PingCommand),
    Name(NameCommand),
    Replicate(ReplicateCommand),
    UserSync(UserSyncCommand),
    FederationAck(FederationAckCommand),
    Sync(SyncCommand),
    RemovePusher(RemovePusherCommand),
    InvalidateCache(InvalidateCacheCommand),
    UserIp(UserIpCommand),
    RemoteServerUp(RemoteServerUpCommand),
    Unknown(String, String),
}

impl Commands {
    pub fn from_line(line: String) -> Self {
        let mut iter = line.splitn(2, char::is_whitespace);
        let name: String = iter.next().unwrap().to_string();
        let content: String = iter.next().unwrap().to_string();

        match name.as_ref() {
            ServerCommand::NAME => Commands::Server(ServerCommand::from_line(content)),
            RDataCommand::NAME => Commands::RData(RDataCommand::from_line(content)),
            PositionCommand::NAME => Commands::Position(PositionCommand::from_line(content)),
            ErrorCommand::NAME => Commands::Error(ErrorCommand::from_line(content)),
            PingCommand::NAME => Commands::Ping(PingCommand::from_line(content)),
            NameCommand::NAME => Commands::Name(NameCommand::from_line(content)),
            ReplicateCommand::NAME => Commands::Replicate(ReplicateCommand::from_line(content)),
            UserSyncCommand::NAME => Commands::UserSync(UserSyncCommand::from_line(content)),
            FederationAckCommand::NAME => {
                Commands::FederationAck(FederationAckCommand::from_line(content))
            }
            SyncCommand::NAME => Commands::Sync(SyncCommand::from_line(content)),
            RemovePusherCommand::NAME => {
                Commands::RemovePusher(RemovePusherCommand::from_line(content))
            }
            InvalidateCacheCommand::NAME => {
                Commands::InvalidateCache(InvalidateCacheCommand::from_line(content))
            }
            UserIpCommand::NAME => Commands::UserIp(UserIpCommand::from_line(content)),
            _ => Commands::Unknown(name, line),
        }
    }
}

pub trait Command: std::fmt::Display {
    const NAME: &'static str;

    fn from_line(line: String) -> Self;
    fn to_line(&self) -> String;
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct ServerCommand {
    pub name: String,
}

impl Command for ServerCommand {
    const NAME: &'static str = "SERVER";

    fn from_line(line: String) -> Self {
        ServerCommand { name: line }
    }

    fn to_line(&self) -> String {
        self.name.clone()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct RDataCommand {
    pub stream: String,
    pub token: Option<u64>,
    pub row_data: StreamRows,
}

impl RDataCommand {
    pub fn is_batched(&self) -> bool {
        self.token.is_none()
    }
}

impl Command for RDataCommand {
    const NAME: &'static str = "RDATA";

    fn from_line(line: String) -> Self {
        let mut iter = line.splitn(3, char::is_whitespace);
        let stream = iter.next().unwrap().to_string();
        let token: Option<u64> = if let Ok(t) = iter.next().unwrap().parse::<u64>() {
            Some(t)
        } else {
            None
        };
        let rdata: String = iter.next().unwrap().to_string();

        let row_data: StreamRows = match stream.as_str() {
            EventStreamRow::NAME => StreamRows::Event(EventStreamRow::from_raw(rdata)),
            FederationStreamRow::NAME => {
                StreamRows::Federation(FederationStreamRow::from_raw(rdata))
            }
            _ => StreamRows::Unknown(rdata),
        };

        RDataCommand {
            stream,
            token,
            row_data,
        }
    }

    fn to_line(&self) -> String {
        if self.is_batched() {
            format!("{} batch {}", self.stream, self.row_data)
        } else {
            format!("{} {} {}", self.stream, self.token.unwrap(), self.row_data)
        }
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct PositionCommand {
    pub stream: String,
    pub token: u64,
}

impl Command for PositionCommand {
    const NAME: &'static str = "POSITION";

    fn from_line(line: String) -> Self {
        let mut iter = line.splitn(2, char::is_whitespace);
        let stream = iter.next().unwrap().to_string();
        let token = iter.next().unwrap().parse::<u64>().unwrap();

        PositionCommand { stream, token }
    }

    fn to_line(&self) -> String {
        format!("{} {}", self.stream, self.token)
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct ErrorCommand {}

impl Command for ErrorCommand {
    const NAME: &'static str = "ERROR";

    fn from_line(line: String) -> Self {
        ErrorCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct PingCommand {
    pub millis_since_epoch: u64,
}

impl Command for PingCommand {
    const NAME: &'static str = "PING";

    fn from_line(line: String) -> Self {
        PingCommand {
            millis_since_epoch: line.parse::<u64>().unwrap(),
        }
    }

    fn to_line(&self) -> String {
        self.millis_since_epoch.to_string()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct NameCommand {
    pub name: String,
}

impl Command for NameCommand {
    const NAME: &'static str = "NAME";

    fn from_line(line: String) -> Self {
        NameCommand { name: line }
    }

    fn to_line(&self) -> String {
        self.name.clone()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct ReplicateCommand {
    pub stream: String,
    pub token: u64,
}

impl Command for ReplicateCommand {
    const NAME: &'static str = "REPLICATE";

    fn from_line(line: String) -> Self {
        let mut iter = line.splitn(2, char::is_whitespace);
        let stream = iter.next().unwrap().to_string();
        let token = iter.next().unwrap().parse::<u64>().unwrap();

        ReplicateCommand { stream, token }
    }

    fn to_line(&self) -> String {
        if self.token == 0 {
            format!("{} NOW", self.stream)
        } else {
            format!("{} {}", self.stream, self.token)
        }
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct UserSyncCommand {}

impl Command for UserSyncCommand {
    const NAME: &'static str = "USER_SYNC";

    fn from_line(line: String) -> Self {
        UserSyncCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct FederationAckCommand {
    pub token: u64,
}

impl Command for FederationAckCommand {
    const NAME: &'static str = "FEDERATION_ACK";

    fn from_line(line: String) -> Self {
        FederationAckCommand {
            token: line.parse::<u64>().unwrap(),
        }
    }

    fn to_line(&self) -> String {
        format!("{}", self.token)
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct SyncCommand {}

impl Command for SyncCommand {
    const NAME: &'static str = "SYNC";

    fn from_line(line: String) -> Self {
        SyncCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct RemovePusherCommand {}

impl Command for RemovePusherCommand {
    const NAME: &'static str = "REMOVE_PUSHER";

    fn from_line(line: String) -> Self {
        RemovePusherCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct InvalidateCacheCommand {}

impl Command for InvalidateCacheCommand {
    const NAME: &'static str = "INVALIDATE_CACHE";

    fn from_line(line: String) -> Self {
        InvalidateCacheCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct UserIpCommand {}

impl Command for UserIpCommand {
    const NAME: &'static str = "USER_IP";

    fn from_line(line: String) -> Self {
        UserIpCommand {}
    }

    fn to_line(&self) -> String {
        String::new()
    }
}

#[derive(Debug, Clone, CommandDisplay)]
pub struct RemoteServerUpCommand {
    pub server_name: String,
}

impl Command for RemoteServerUpCommand {
    const NAME: &'static str = "REMOTE_SERVER_UP";

    fn from_line(line: String) -> Self {
        RemoteServerUpCommand {
            server_name: line,
        }
    }

    fn to_line(&self) -> String {
        self.server_name.clone()
    }
}