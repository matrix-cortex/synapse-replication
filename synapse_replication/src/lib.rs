pub mod commands;
pub mod connection;
pub mod edu_event;
pub mod presence_state;
pub mod streams;

#[cfg(test)]
mod tests {
    use crate::connection::{Connection, ConnectionError, ReplicationLine, RowsData};

    use futures::StreamExt;

    #[tokio::test]
    async fn connect_failure() {
        match Connection::new(
            "test-replication".to_string(),
            "example.org".to_string(),
            "example.org".to_string(),
            2345,
        )
        .await
        {
            Ok(_) => panic!("connection unexpectedly succeeded"),
            Err(e) => {
                assert_eq!(e, ConnectionError::TcpTimeout);
            }
        };

        match Connection::new(
            "test-replication".to_string(),
            "example.org".to_string(),
            "example.example".to_string(),
            2345,
        )
        .await
        {
            Ok(_) => panic!("connection unexpectedly succeeded"),
            Err(e) => {
                assert_eq!(e, ConnectionError::TcpDisconnected);
            }
        };
    }

    #[tokio::test]
    async fn connect_successful() {
        match Connection::new(
            "test-replication".to_string(),
            "thinker.eu.org".to_string(),
            "thinker.eu.org".to_string(),
            9092,
        )
        .await
        {
            Ok(_) => {}
            Err(e) => panic!("Connection unexpectedly ended: {}", e),
        }
    }

    #[tokio::test]
    async fn replication_stream() {
        let (mut conn, background) = Connection::new(
            "test-replication".to_string(),
            "thinker.eu.org".to_string(),
            "thinker.eu.org".to_string(),
            9092,
        )
        .await
        .unwrap();

        tokio::spawn(background);

        if let Some(result) = conn.next().await {
            if let Ok(replication_line) = result {
                println!("{:?}", replication_line);
                return;
            }
        }

        panic!("Replication failed")
    }
}
